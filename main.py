# sudo python main.py packetfile.default http://mirror.yandex.ru/debian/ testing 
import argparse
import sys
import os
import apt

parser = argparse.ArgumentParser(description = 'First argument - packet list file, second - mirror url, third - stage')
parser.add_argument('packetfile')
parser.add_argument('mirror_url')
parser.add_argument('stage', choices=['stable', 'unstable', 'testing'])

args = parser.parse_args()

try:
    with open(args.packetfile) as f:
        packet_name_list=[line.rstrip() for line in f]
except OSError:
    print("Packet file {} does not exists or can't be read.".format(args.packetfile))
    sys.exit(1)

mirror_url = args.mirror_url
stage = args.stage

# Call debootstrap
try:
    os.mkdir('debian')
    os.system('debootstrap --variant=buildd --include=gnupg '+stage+' ./debian '+mirror_url)
except OSError:
    print("No bootstrap was created")

# From this moment we work in chroot
os.chroot('./debian')

# Add src line to /etc/apt/sources.list
src_line = 'deb-src '+mirror_url+' '+stage+' main\n'
with open('/etc/apt/sources.list', 'a') as src_file:
    src_file.write(src_line)

# Update: its easier that way, really
os.system('cd ; apt-key net-update; apt-get update')

# Get all the depends list:
def get_all_depends(cache, pkg):
    candidate_version = cache._depcache.get_candidate_ver(pkg._pkg)
    depends_list = candidate_version.depends_list
# Get build depends for every packet
cache = apt.Cache()
src_records = apt.apt_pkg.SourceRecords()
pkg = cache[packet_name_list[0]]

dep_cache = apt.apt_pkg.DepCache(apt.apt_pkg.Cache())
